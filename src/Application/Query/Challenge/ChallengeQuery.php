<?php
declare(strict_types=1);

namespace App\Application\Query\Challenge;

class ChallengeQuery
{
    private string $challengeId;

    public function __construct(string $challengeId)
    {
        $this->challengeId = $challengeId;
    }

    public function getId()
    {
        return $this->challengeId;
    }
}

