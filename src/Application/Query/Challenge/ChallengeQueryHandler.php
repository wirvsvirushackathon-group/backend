<?php
declare(strict_types=1);

namespace App\Application\Query\Challenge;

use App\Domain\ChallengeId;
use App\Domain\ChallengeRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ChallengeQueryHandler implements MessageHandlerInterface
{
    private ChallengeRepositoryInterface $challengeRepository;

    public function __construct(ChallengeRepositoryInterface $challengeRepository)
    {
        $this->challengeRepository = $challengeRepository;
    }

    public function __invoke(ChallengeQuery $challengeQuery): array
    {
        $challenge = $this->challengeRepository->loadChallenge(ChallengeId::fromString($challengeQuery->getId()));

        return [
            'name' => $challenge->getName(),
            'text' => $challenge->getText(),
            'type' => $challenge->getType(),
        ];
    }
}

