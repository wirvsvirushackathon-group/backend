<?php
declare(strict_types=1);

namespace App\Application\Query\SolutionList;

use App\Domain\ChallengeSolutionRepositoryInterface;

class SolutionListQuery
{
    private string $challengeId;
    private int $offset;
    private int $limit;

    public function __construct(string $challengesId, int $offset, int $limit)
    {
        $this->challengeId = $challengesId;
        $this->offset = $offset;
        $this->limit = $limit;
    }

    public function getChallengeId(): string
    {
        return $this->challengeId;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }
}

