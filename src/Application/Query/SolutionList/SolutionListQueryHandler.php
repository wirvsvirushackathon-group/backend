<?php
declare(strict_types=1);

namespace App\Application\Query\SolutionList;

use App\Domain\ChallengeId;
use App\Domain\ChallengeRepositoryInterface;
use App\Domain\ChallengeSolution;
use App\Domain\ChallengeSolutionRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SolutionListQueryHandler implements MessageHandlerInterface
{
    private ChallengeSolutionRepositoryInterface $challengeSolutionRepository;
    private ChallengeRepositoryInterface $challengeRepository;

    public function __construct(
        ChallengeRepositoryInterface $challengeRepository,
        ChallengeSolutionRepositoryInterface $challengeSolutionRepository
    ) {
        $this->challengeSolutionRepository = $challengeSolutionRepository;
        $this->challengeRepository = $challengeRepository;
    }

    public function __invoke(SolutionListQuery $solutionListQuery): array
    {
        $challenge = $this->challengeRepository->loadChallenge(
            ChallengeId::fromString($solutionListQuery->getChallengeId())
        );
        $solutionList = $this->challengeSolutionRepository->loadList(
            [
                'challenge' => $challenge
            ],
            $solutionListQuery->getOffset(),
            $solutionListQuery->getLimit()
        );

        $result = [];
        foreach ($solutionList as $solution) {
            $result[] = [
                'id' => $solution->getId()->toString(),
                'image_url' => '/solution-images/' . $solution->getImagePath(),
                'submitterId' => $solution->getSubmitter()->getId()->toString(),
                'challengeId' => $solution->getChallenge()->getId()->toString(),
            ];
        }

        return $result;
    }
}

