<?php
declare(strict_types=1);

namespace App\Application\Query\ChallengeList;

use App\Domain\Challenge;
use App\Domain\ChallengeRepositoryInterface;
use App\Domain\ChallengeSolutionRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ChallengeListQueryHandler implements MessageHandlerInterface
{
    private ChallengeRepositoryInterface $challengeRepository;
    private ChallengeSolutionRepositoryInterface $challengeSolutionRepository;

    public function __construct(
        ChallengeRepositoryInterface $challengeRepository,
        ChallengeSolutionRepositoryInterface $challengeSolutionRepository
    ) {
        $this->challengeRepository = $challengeRepository;
        $this->challengeSolutionRepository = $challengeSolutionRepository;
    }

    public function __invoke(ChallengeListQuery $challengeListQuery): array
    {
        $type = $challengeListQuery->getType();

        $criteria = [];
        if ($type !== '') {
            $criteria = ['type' => $type];
        }

        $challenges = $this->challengeRepository->loadList(
            $criteria,
            $challengeListQuery->getOffset(),
            $challengeListQuery->getLimit()
        );

        $challengesList = [];
        foreach ($challenges as $challenge) {
            $challengeEntry = [
                'id'   => $challenge->getId()->toString(),
                'name' => $challenge->getName(),
                'text' => $challenge->getText(),
                'type' => $challenge->getType(),
            ];
            if ($challengeListQuery->isWithSolutions()) {
                $solutions = $this->getSolutions($challenge);
                $challengeEntry['solutions'] = $solutions;
            }
            $challengesList[] = $challengeEntry;
        }

        return $challengesList;
    }

    /**
     * @return string[]
     */
    private function getSolutions(Challenge $challenge): array
    {
        $solutionList = $this->challengeSolutionRepository->loadList(
            [
                'challenge' => $challenge
            ],
            0,
            10
        );
        $solutions = [];
        foreach ($solutionList as $solution) {
            $solutions[] = [
                'id' => $solution->getId()->toString(),
                'submitter' => $solution->getSubmitter()->getUsername(),
                'image_url' => '/solution-images/' . $solution->getImagePath(),
            ];
        }
        return $solutions;
    }
}

