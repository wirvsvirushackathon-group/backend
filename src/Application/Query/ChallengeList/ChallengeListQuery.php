<?php
declare(strict_types=1);

namespace App\Application\Query\ChallengeList;

class ChallengeListQuery
{
    private string $type;
    private int $offset;
    private int $limit;
    private bool $withSolutions;

    public function __construct(string $type, int $offset, int $limit, bool $withSolutions)
    {
        $this->type = $type;
        $this->offset = $offset;
        $this->limit = $limit;
        $this->withSolutions = $withSolutions;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function isWithSolutions(): bool
    {
        return $this->withSolutions;
    }
}

