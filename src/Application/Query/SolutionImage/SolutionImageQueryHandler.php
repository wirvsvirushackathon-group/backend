<?php
declare(strict_types=1);

namespace App\Application\Query\SolutionImage;

use App\Infrastructure\Storage\FileStorage;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SolutionImageQueryHandler implements MessageHandlerInterface
{
    private FileStorage $imageStorage;

    public function __construct(FileStorage $imageStorage)
    {
        $this->imageStorage = $imageStorage;
    }

    public function __invoke(SolutionImageQuery $solutionImageQuery)
    {
        return $this->imageStorage->loadFile($solutionImageQuery->getPath());
    }
}
