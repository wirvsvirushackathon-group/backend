<?php
declare(strict_types=1);

namespace App\Application\Query\SolutionImage;

class SolutionImageQuery
{
    private string $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function getPath()
    {
        return $this->filePath;
    }
}
