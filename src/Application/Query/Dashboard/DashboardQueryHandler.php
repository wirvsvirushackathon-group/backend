<?php
declare(strict_types=1);

namespace App\Application\Query\Dashboard;

use App\Domain\ChallengeRepositoryInterface;
use App\Domain\UserId;
use App\Domain\UserRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class DashboardQueryHandler implements MessageHandlerInterface
{
    private UserRepositoryInterface $userRepository;
    private ChallengeRepositoryInterface $challengeRepository;

    public function __construct(
        UserRepositoryInterface $userRepository,
        ChallengeRepositoryInterface $challengeRepository
    ) {
        $this->userRepository = $userRepository;
        $this->challengeRepository = $challengeRepository;
    }

    public function __invoke(DashboardQuery $dashboardQuery): array
    {
        $user = $this->userRepository->loadByUserId(UserId::fromString($dashboardQuery->getUserId()));

        if ($user === null) {
            throw new \Exception('User data can not be found.');
        }

        $userChallenges = $this->challengeRepository->loadChallengesByUser($user);

        $userChallengesList = [];
        foreach ($userChallenges as $userChallenge) {
            $userChallengesList[] = [
                'id' => $userChallenge->getId()->toString(),
                'title' => $userChallenge->getName(),
            ];
        }

        return [
            'username' => $user->getUsername(),
            'points' => $user->getPoints(),
            'challenges' => $userChallengesList,
        ];
    }
}

