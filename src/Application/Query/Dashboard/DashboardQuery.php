<?php
declare(strict_types=1);

namespace App\Application\Query\Dashboard;

class DashboardQuery
{
    private string $userId;

    public function __construct(string $userId)
    {
        $this->userId = $userId;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }
}

