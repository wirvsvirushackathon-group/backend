<?php
declare(strict_types=1);

namespace App\Application\Command\InitState;

use App\Domain\Challenge;
use App\Domain\ChallengeRepositoryInterface;
use App\Domain\ChallengeSolution;
use App\Domain\ChallengeSolutionRepositoryInterface;
use App\Domain\User;
use App\Domain\UserRepositoryInterface;
use App\Infrastructure\Storage\FileStorage;
use App\Infrastructure\Storage\WrongTypeException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class InitStateCommandHandler implements MessageHandlerInterface
{
    private UserRepositoryInterface $userRepository;
    private ChallengeRepositoryInterface $challengeRepository;
    private ChallengeSolutionRepositoryInterface $challengeSolutionRepository;
    private FileStorage $imageStorage;

    public function __construct(
        UserRepositoryInterface $userRepository,
        ChallengeRepositoryInterface $challengeRepository,
        ChallengeSolutionRepositoryInterface $challengeSolutionRepository,
        FileStorage $imageStorage
    ) {
        $this->userRepository = $userRepository;
        $this->challengeRepository = $challengeRepository;
        $this->challengeSolutionRepository = $challengeSolutionRepository;
        $this->imageStorage = $imageStorage;
    }

    /**
     * @throws WrongTypeException
     */
    public function __invoke(InitStateCommand $initStateCommand): void
    {
        $adminUser = $this->registerAdminUser(
            $initStateCommand->getAdminUsername(),
            $initStateCommand->getAdminEmail(),
            $initStateCommand->getAdminPassword(),
        );

        $jsonString = file_get_contents($initStateCommand->getStartChallengesFilePath());
        $listOfStartChallenges = json_decode($jsonString, true);

        $this->submitStartChallenges($adminUser, $listOfStartChallenges['challenges']);
    }

    private function registerAdminUser(string $username, string $email, string $password): User
    {
        $adminUser = User::registerAdmin($this->userRepository->getNextId(), $username, $email, $password);
        return $this->userRepository->persist($adminUser);
    }

    /**
     * @param mixed[] $listOfStartChallenges
     * @throws WrongTypeException
     */
    private function submitStartChallenges(User $adminUser, array $listOfStartChallenges): void
    {
        foreach ($listOfStartChallenges as $challengeEntry) {
            $challenge = Challenge::submit(
                $this->challengeRepository->getNextId(),
                $adminUser,
                $challengeEntry['title'],
                $challengeEntry['description'],
                'onboarding'
            );
            $this->challengeRepository->persist($challenge);
            foreach ($challengeEntry['solutions'] as $solutionEntry) {
                $imagePath = $this->imageStorage->uploadFileFromWebSource($adminUser, $solutionEntry['image']);
                $solution = ChallengeSolution::submit(
                    $this->challengeSolutionRepository->getNextId(),
                    $adminUser,
                    $challenge,
                    $imagePath
                );
                $this->challengeSolutionRepository->persist($solution);
            }
        }
    }

}

