<?php
declare(strict_types=1);

namespace App\Application\Command\InitState;

class InitStateCommand
{
    private string $adminUsername;
    private string $adminEmail;
    private string $adminPassword;
    private string $startChallengesFilePath;

    public function __construct(
        string $adminUsername,
        string $adminEmail,
        string $adminPassword,
        string $startChallengesFilePath
    ) {
        $this->adminUsername = $adminUsername;
        $this->adminEmail = $adminEmail;
        $this->adminPassword = $adminPassword;
        $this->startChallengesFilePath = $startChallengesFilePath;
    }

    public function getAdminUsername(): string
    {
        return $this->adminUsername;
    }

    public function getAdminEmail(): string
    {
        return $this->adminEmail;
    }

    public function getAdminPassword(): string
    {
        return $this->adminPassword;
    }

    public function getStartChallengesFilePath(): string
    {
        return $this->startChallengesFilePath;
    }
}

