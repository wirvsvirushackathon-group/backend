<?php
declare(strict_types=1);

namespace App\Application\Command\SubmitChallenge;

class SubmitChallengeCommand
{
    private string $username;
    private string $name;
    private string $text;
    private string $type;

    public function __construct(string $username, string $name, string $text, string $type = '')
    {
        $this->username = $username;
        $this->name = $name;
        $this->text = $text;
        $this->type = $type;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getType(): string
    {
        return $this->type;
    }
}

