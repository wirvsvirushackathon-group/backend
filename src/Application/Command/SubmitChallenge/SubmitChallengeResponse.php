<?php
declare(strict_types=1);

namespace App\Application\Command\SubmitChallenge;

use App\Domain\Challenge;

class SubmitChallengeResponse
{
    public ?string $message;
    private ?Challenge $challenge;
    private bool $isError = false;

    public static function success(Challenge $challenge): self
    {
        $response = new static();
        $response->challenge = $challenge;

        return $response;
    }

    public static function challengeAlreadyExists(Challenge $challenge): self
    {
        $response = new static();
        $response->message = sprintf('It exists already a challenge with the name %s.', $challenge->getName());
        $response->isError = true;

        return $response;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function isError(): bool
    {
        return $this->isError;
    }

    public function getChallenge(): ?Challenge
    {
        return $this->challenge;
    }
}

