<?php
declare(strict_types=1);

namespace App\Application\Command\SubmitChallenge;

use App\Domain\Challenge;
use App\Domain\ChallengeRepositoryInterface;
use App\Domain\UserRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SubmitChallengeCommandHandler implements MessageHandlerInterface
{
    private UserRepositoryInterface $userRepository;
    private ChallengeRepositoryInterface $challengeRepository;

    public function __construct(
        UserRepositoryInterface $userRepository,
        ChallengeRepositoryInterface $challengeRepository
    ) {
        $this->userRepository = $userRepository;
        $this->challengeRepository = $challengeRepository;
    }

    public function __invoke(SubmitChallengeCommand $submitChallengeCommand): SubmitChallengeResponse
    {
        $user = $this->userRepository->loadByUsername($submitChallengeCommand->getUsername());
        $challenge = Challenge::submit(
            $this->challengeRepository->getNextId(),
            $user,
            $submitChallengeCommand->getName(),
            $submitChallengeCommand->getText(),
            $submitChallengeCommand->getType()
        );

        $existingChallenge = $this->challengeRepository->loadChallengeByName($submitChallengeCommand->getName());

        if ($existingChallenge !== null) {
            return SubmitChallengeResponse::challengeAlreadyExists($existingChallenge);
        }

        $challenge = $this->challengeRepository->persist($challenge);
        $this->userRepository->persist($user->addPoints($challenge->getEarnedPoints()));

        return SubmitChallengeResponse::success($challenge);
    }
}

