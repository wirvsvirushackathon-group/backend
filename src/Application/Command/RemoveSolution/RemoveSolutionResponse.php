<?php
declare(strict_types=1);

namespace App\Application\Command\RemoveSolution;

class RemoveSolutionResponse
{
    public ?string $message;
    private int $lostPoints = 0;
    private bool $isError = false;

    public static function success(int $lostPoints): self
    {
        $response = new static();
        $response->lostPoints = $lostPoints;

        return $response;
    }

    public static function solutionDoesNotExists(): self
    {
        $response = new static();
        $response->message = 'No solutions for given id exists.';
        $response->isError = true;

        return $response;
    }

    public static function userIsNotPermitted(): self
    {
        $response = new static();
        $response->message = 'You have no permissions to delete the given solution.';
        $response->isError = true;

        return $response;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function isError(): bool
    {
        return $this->isError;
    }

    public function getLostPoints(): int
    {
        return $this->lostPoints;
    }
}

