<?php
declare(strict_types=1);

namespace App\Application\Command\RemoveSolution;

use App\Domain\ChallengeSolutionId;
use App\Domain\ChallengeSolutionRepositoryInterface;
use App\Domain\UserId;
use App\Domain\UserRepositoryInterface;
use App\Infrastructure\Storage\FileStorage;
use League\Flysystem\FileNotFoundException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class RemoveSolutionCommandHandler implements MessageHandlerInterface
{
    private UserRepositoryInterface $userRepository;
    private FileStorage $imageStorage;
    private ChallengeSolutionRepositoryInterface $challengeSolutionRepository;

    public function __construct(
        UserRepositoryInterface $userRepository,
        ChallengeSolutionRepositoryInterface $challengeSolutionRepository,
        FileStorage $imageStorage
    ) {
        $this->userRepository = $userRepository;
        $this->imageStorage = $imageStorage;
        $this->challengeSolutionRepository = $challengeSolutionRepository;
    }

    /**
     * @throws FileNotFoundException
     */
    public function __invoke(RemoveSolutionCommand $deleteSolutionCommand): RemoveSolutionResponse
    {
        $userId = UserId::fromString($deleteSolutionCommand->getUserId());
        $solutionId = ChallengeSolutionId::fromString($deleteSolutionCommand->getSolutionId());

        $user = $this->userRepository->loadByUserId($userId);
        $solution = $this->challengeSolutionRepository->loadSolution($solutionId);

        if ($solution === null) {
            return RemoveSolutionResponse::solutionDoesNotExists();
        }

        if (!in_array('ROLE_ADMIN', $user->getRoles()) && !$solution->isSubmitter($user)) {
            return RemoveSolutionResponse::userIsNotPermitted();
        }

        $solutionImagePath = $solution->getImagePath();
        try {
            $this->imageStorage->removeFile($solutionImagePath);
        } catch (FileNotFoundException $e) {
        }


        $this->challengeSolutionRepository->remove($solution);
        $this->userRepository->persist($user->removePoints($solution->getEarnedPoints()));

        return RemoveSolutionResponse::success($solution->getEarnedPoints());
    }
}

