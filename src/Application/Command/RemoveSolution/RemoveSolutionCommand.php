<?php
declare(strict_types=1);

namespace App\Application\Command\RemoveSolution;

class RemoveSolutionCommand
{
    private string $userId;
    private string $solutionId;

    public function __construct(string $userId, string $solutionId)
    {
        $this->userId = $userId;
        $this->solutionId = $solutionId;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getSolutionId(): string
    {
        return $this->solutionId;
    }
}

