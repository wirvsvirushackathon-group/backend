<?php
declare(strict_types=1);

namespace App\Application\Command\RegisterUser;

use App\Domain\User;
use App\Domain\UserRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class RegisterUserCommandHandler implements MessageHandlerInterface
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(RegisterUserCommand $registerUserCommand): RegistrationResponse
    {
        $existingUserWithEmail = $this->userRepository->loadByEmail($registerUserCommand->getEmail());
        $existingUserWithUsername = $this->userRepository->loadByUsername($registerUserCommand->getUsername());

        if ($existingUserWithEmail !== null) {
            return RegistrationResponse::emailAlreadyExists();
        }

        if ($existingUserWithUsername !== null) {
            return RegistrationResponse::usernameAlreadyExists();
        }

        $registeredUser = $this->userRepository->persist(
            User::register(
                $this->userRepository->getNextId(),
                $registerUserCommand->getUsername(),
                $registerUserCommand->getEmail(),
                $registerUserCommand->getPassword()
            )
        );
        $token = $this->userRepository->getUserToken($registeredUser);

        return RegistrationResponse::success($registeredUser, $token);
    }
}

