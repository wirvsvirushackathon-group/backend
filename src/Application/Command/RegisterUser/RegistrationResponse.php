<?php
declare(strict_types=1);

namespace App\Application\Command\RegisterUser;

use App\Domain\User;

class RegistrationResponse
{
    public ?string $message;
    private ?User $user;
    private string $token;
    private bool $isError = false;

    public static function success(User $user, string $token): self
    {
        $response = new static();
        $response->user = $user;
        $response->token = $token;

        return $response;
    }

    public static function emailAlreadyExists(): self
    {
        $response = new static();
        $response->message = 'Ein Benutzer mit der angegebenen Email existiert bereits.';
        $response->isError = true;

        return $response;
    }

    public static function usernameAlreadyExists()
    {
        $response = new static();
        $response->message = 'Ein Benutzer mit dem angegebenen Username existiert bereits.';
        $response->isError = true;

        return $response;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function isError(): bool
    {
        return $this->isError;
    }
}

