<?php
declare(strict_types=1);

namespace App\Application\Command\SubmitSolution;

use App\Domain\ChallengeSolution;

class SolutionSubmitResponse
{
    public ?string $message;
    private ?ChallengeSolution $challengeSolution;
    private bool $isError = false;

    public static function success(ChallengeSolution $challengeSolution): self
    {
        $response = new static();
        $response->challengeSolution = $challengeSolution;

        return $response;
    }

    public static function solutionAlreadyExists(): self
    {
        $response = new static();
        $response->message = 'It exists already a solution for this challenge.';
        $response->isError = true;

        return $response;
    }

    public static function challengeDoesNotExists(): self
    {
        $response = new static();
        $response->message = 'There is not challenge for the given challenge id.';
        $response->isError = true;

        return $response;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getChallengeSolution(): ?ChallengeSolution
    {
        return $this->challengeSolution;
    }

    public function isError(): bool
    {
        return $this->isError;
    }
}

