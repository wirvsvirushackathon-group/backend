<?php
declare(strict_types=1);

namespace App\Application\Command\SubmitSolution;

use App\Domain\ChallengeId;
use App\Domain\ChallengeRepositoryInterface;
use App\Domain\ChallengeSolution;
use App\Domain\ChallengeSolutionRepositoryInterface;
use App\Domain\UserId;
use App\Domain\UserRepositoryInterface;
use App\Infrastructure\Storage\FileStorage;
use App\Infrastructure\Storage\WrongTypeException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SubmitSolutionCommandHandler implements MessageHandlerInterface
{
    private UserRepositoryInterface $userRepository;
    private FileStorage $imageStorage;
    private ChallengeRepositoryInterface $challengeRepository;
    private ChallengeSolutionRepositoryInterface $challengeSolutionRepository;

    public function __construct(
        UserRepositoryInterface $userRepository,
        ChallengeRepositoryInterface $challengeRepository,
        ChallengeSolutionRepositoryInterface $challengeSolutionRepository,
        FileStorage $imageStorage
    ) {
        $this->userRepository = $userRepository;
        $this->imageStorage = $imageStorage;
        $this->challengeRepository = $challengeRepository;
        $this->challengeSolutionRepository = $challengeSolutionRepository;
    }

    /**
     * @throws WrongTypeException
     */
    public function __invoke(SubmitSolutionCommand $submitSolutionCommand): SolutionSubmitResponse
    {
        $userId = UserId::fromString($submitSolutionCommand->getUserId());
        $challengeId = ChallengeId::fromString($submitSolutionCommand->getChallengeId());

        $user = $this->userRepository->loadByUserId($userId);
        $challenge = $this->challengeRepository->loadChallenge($challengeId);

        if ($challenge === null) {
            return SolutionSubmitResponse::challengeDoesNotExists();
        }

        $challengeSolution = $this->challengeSolutionRepository->loadSolutionsByUserAndChallenge($user, $challenge);

        if ($challengeSolution !== null) {
            return SolutionSubmitResponse::solutionAlreadyExists();
        }

        $newFilePath = $this->imageStorage->uploadFile($user, $submitSolutionCommand->getFilePath());
        $challengeId = $this->challengeSolutionRepository->getNextId();
        $challengeSolution = $this->challengeSolutionRepository
            ->persist(ChallengeSolution::submit($challengeId, $user, $challenge, $newFilePath));
        $this->userRepository->persist($user->addPoints($challengeSolution->getEarnedPoints()));

        return SolutionSubmitResponse::success($challengeSolution);
    }
}

