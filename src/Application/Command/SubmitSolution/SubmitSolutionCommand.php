<?php
declare(strict_types=1);

namespace App\Application\Command\SubmitSolution;

class SubmitSolutionCommand
{
    private string $userId;
    private string $challengeId;
    private string $filePath;

    public function __construct(string $userId, string $challengeId, string $filePath)
    {
        $this->userId = $userId;
        $this->challengeId = $challengeId;
        $this->filePath = $filePath;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getChallengeId(): string
    {
        return $this->challengeId;
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }
}

