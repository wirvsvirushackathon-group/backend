<?php
declare(strict_types=1);

namespace App\Application\Bus;

interface QueryBusInterface
{
    /**
     * @param object $query
     *
     * @return mixed The handler returned value
     */
    public function query($query);
}

