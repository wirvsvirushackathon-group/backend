<?php
declare(strict_types=1);

namespace App\Application\Bus;

interface CommandBusInterface
{
    /**
     * @param object $command
     *
     * @return mixed The handler returned value
     */
    public function dispatch($command);
}

