<?php

namespace App\Infrastructure\Controller;

use App\Application\Bus\CommandBusInterface;
use App\Application\Bus\QueryBusInterface;
use App\Application\Command\RegisterUser\RegisterUserCommand;
use App\Application\Query\Dashboard\DashboardQuery;
use App\Domain\User;
use App\Infrastructure\Validation\UserRegistrationValidator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    private CommandBusInterface $commandBus;
    private QueryBusInterface $queryBus;

    public function __construct(CommandBusInterface $commandBus, QueryBusInterface $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }

    /**
     * @Route("/users/register", name="register_user", methods={"POST"})
     */
    public function register(Request $request, UserRegistrationValidator $validator)
    {
        $data = json_decode($request->getContent(),true);

        $violations = $validator->validate($data);
        if ($violations !== []) {
            return new JsonResponse(['errors' => $violations], Response::HTTP_BAD_REQUEST);
        }

        $registrationResponse = $this->commandBus->dispatch(
            new RegisterUserCommand(
                $data['username'],
                $data['email'],
                $data['password']
            )
        );
        if ($registrationResponse->isError()) {
            return new JsonResponse(
                [
                    'status' => $registrationResponse->getMessage(),
                ],
                Response::HTTP_NOT_ACCEPTABLE
            );
        }

        return new JsonResponse(
            [
                'status' => 'User registered',
                'token' => $registrationResponse->getToken(),
            ],
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/users/dashboard", name="dashboard", methods={"GET"})
     */
    public function dashboard(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        try {
            $dashboardResult = $this->queryBus->query(new DashboardQuery($user->getId()->toString()));
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($dashboardResult, Response::HTTP_OK);
    }
}
