<?php

namespace App\Infrastructure\Controller;

use App\Application\Bus\CommandBusInterface;
use App\Application\Bus\QueryBusInterface;
use App\Application\Command\RemoveSolution\RemoveSolutionCommand;
use App\Application\Command\RemoveSolution\RemoveSolutionResponse;
use App\Application\Command\SubmitSolution\SolutionSubmitResponse;
use App\Application\Command\SubmitSolution\SubmitSolutionCommand;
use App\Application\Query\SolutionList\SolutionListQuery;
use App\Domain\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ChallengeSolutionController extends AbstractController
{
    private CommandBusInterface $commandBus;
    private QueryBusInterface $queryBus;

    public function __construct(CommandBusInterface $commandBus, QueryBusInterface $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }

    /**
     * @Route("/challenges/{challengeId}/solutions", name="submit_solution", methods={"POST"})
     */
    public function submitSolution(
        Request $request,
        string $challengeId
    ) {
        /** @var UploadedFile $file */
        $file = $request->files->get('file');

        if ($file->getSize() > 1000000) {
            return $this->json([
                'error' => 'The uploaded image is bigger than 1mb. Please upload a smaller image.',
            ]);
        }

        /** @var User $user */
        $user = $this->getUser();

        try {
            /** @var SolutionSubmitResponse $solutionSubmitResponse */
            $solutionSubmitResponse = $this->commandBus->dispatch(
                new SubmitSolutionCommand($user->getId()->toString(), $challengeId, $file->getRealPath())
            );
        } catch (\Exception $e) {
            return $this->json([
                'error' => $e->getMessage(),
            ]);
        }

        if ($solutionSubmitResponse->isError()) {
            return $this->json([
                'error' => $solutionSubmitResponse->getMessage(),
            ]);
        }

        return $this->json([
            'status' => 'solution submitted',
            'earned_points' => $solutionSubmitResponse->getChallengeSolution()->getEarnedPoints(),
        ]);
    }

    /**
     * @Route("/challenges/{challengeId}/solutions", name="get_solution_for_challenge", methods={"GET"})
     */
    public function solutions(string $challengeId)
    {
        $solutionList = $this->queryBus->query(new SolutionListQuery($challengeId, 0, 1000));

        if ($solutionList === []) {
            return new JsonResponse([
                "info" => 'No solutions found.',
            ]);
        }

        return new JsonResponse([
            "solutions" => $solutionList
        ]);
    }

    /**
     * @Route("/solutions/{solutionId}", name="remove_solution", methods={"DELETE"})
     */
    public function removeSolution(string $solutionId)
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var RemoveSolutionResponse $removeSolutionResponse */
        $removeSolutionResponse = $this->commandBus->dispatch(
            new RemoveSolutionCommand($user->getId()->toString(), $solutionId)
        );

        if ($removeSolutionResponse->isError()) {
            return new JsonResponse([
                "error" => $removeSolutionResponse->getMessage(),
            ]);
        }

        return $this->json([
            'status' => 'solution removed',
            'lost_points' => $removeSolutionResponse->getLostPoints(),
        ]);
    }
}