<?php

namespace App\Infrastructure\Controller;

use App\Application\Bus\CommandBusInterface;
use App\Application\Bus\QueryBusInterface;
use App\Application\Command\SubmitChallenge\SubmitChallengeCommand;
use App\Application\Command\SubmitChallenge\SubmitChallengeResponse;
use App\Application\Query\Challenge\ChallengeQuery;
use App\Application\Query\ChallengeList\ChallengeListQuery;
use App\Domain\ChallengeRepositoryInterface;
use App\Infrastructure\Validation\ChallengeCreateValidator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChallengeController extends AbstractController
{
    private CommandBusInterface $commandBus;
    private QueryBusInterface $queryBus;

    public function __construct(CommandBusInterface $commandBus, QueryBusInterface $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }

    /**
     * @Route("/challenges", name="submit_challenge", methods={"POST"})
     */
    public function submitChallenge(
        Request $request,
        ChallengeCreateValidator $validator
    ) {
        $user = $this->getUser();

        if ($user === null) {
            return new JsonResponse(['error' => 'No user found.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $data = json_decode($request->getContent(),true);
        $violations = $validator->validate($data);
        if ($violations !== []) {
            return new JsonResponse(['errors' => $violations], Response::HTTP_BAD_REQUEST);
        }

        /** @var SubmitChallengeResponse $submitChallengeResponse */
        $submitChallengeResponse = $this->commandBus->dispatch(
            new SubmitChallengeCommand(
                $user->getUsername(),
                $data['name'],
                $data['text'],
                $data['type'] ?? '',
            )
        );

        if ($submitChallengeResponse->isError()) {
            return new JsonResponse(['error' => $submitChallengeResponse->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $challenge = $submitChallengeResponse->getChallenge();
        return new JsonResponse(
            [
                'status' => 'Challenge submitted',
                'challenge_id' => $challenge->getId()->toString(),
                'earned_points' => $challenge->getEarnedPoints()
            ],
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/challenges/{challengeId}", name="get_challenge", methods={"GET"})
     */
    public function getChallenge(Request $request, string $challengeId, ChallengeRepositoryInterface $challengeRepository)
    {
        $challenge = $this->queryBus->query(new ChallengeQuery($challengeId));

        if ($challenge === null) {
            return new JsonResponse([
                "error" => 'No challenge for given id found.',
            ]);
        }

        return new JsonResponse(["challenge" => $challenge]);
    }

    /**
     * @Route("/challenges", name="get_challenges", methods={"GET"})
     */
    public function getChallenges(Request $request)
    {
        $type = $request->get('type', '');
        $withSolutions = $request->get('solutions', false);
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 100);

        $challengesResult = $this->queryBus->query(
            new ChallengeListQuery($type, (int) $offset, (int) $limit, (bool) $withSolutions)
        );

        if ($challengesResult === []) {
            return new JsonResponse([
                "info" => 'No challenges found.',
            ]);
        }

        return new JsonResponse(["challenges" => $challengesResult]);
    }
}