<?php

namespace App\Infrastructure\Controller;

use App\Application\Bus\QueryBusInterface;
use App\Application\Query\SolutionImage\SolutionImageQuery;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class ImageController extends AbstractController
{
    private QueryBusInterface $queryBus;

    public function __construct(QueryBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @Route("/solution-images/{userId}/{fileName}", name="image", methods={"GET"})
     */
    public function image(Request $request, string $userId, string $fileName)
    {
        $imageBinaryString = $this->queryBus->query(new SolutionImageQuery($userId . '/' . $fileName));

        $response = new Response();
        $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $fileName);
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'image/jpeg');
        $response->setContent($imageBinaryString);

        return $response;
    }
}
