<?php
declare(strict_types=1);

namespace App\Infrastructure\Cli;

use App\Application\Bus\CommandBusInterface;
use App\Application\Command\InitState\InitStateCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DataInitCommand extends Command
{
    protected static $defaultName = 'init-state';

    private CommandBusInterface $commandBus;

    public function __construct(CommandBusInterface $commandBus, string $name = null)
    {
        parent::__construct($name);
        $this->commandBus = $commandBus;
    }

    protected function configure()
    {
        $this->setDescription('Initialize Database state');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->commandBus->dispatch(new InitStateCommand(
            'admin',
            'admin@homenotalone.de',
            'wefqwdqsegawrvse',
            'config/onboarding/onboarding.json'
        ));

        return 0;
    }
}

