<?php
declare(strict_types=1);

namespace App\Infrastructure\Storage;

use App\Domain\User;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;

class FileStorage
{
    private FilesystemInterface $imageStorage;

    public function __construct(FilesystemInterface $imageStorage)
    {
        $this->imageStorage = $imageStorage;
    }

    /**
     * @throws FileExistsException
     */
    public function uploadFileFromWebSource(User $user, string $url): string
    {
        $ch = curl_init ($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        $fileStream = curl_exec($ch);
        curl_close ($ch);

        $newFilename = hash('sha256', $fileStream) . '.jpg';
        $newFilePath = $user->getId() . '/' . $newFilename;
        $this->imageStorage->write($newFilePath, $fileStream);

        return $newFilePath;
    }

    /**
     * @throws WrongTypeException
     * @throws FileExistsException
     */
    public function uploadFile(User $user, string $filePath): string
    {
        if(!is_array(getimagesize($filePath))){
            throw new WrongTypeException('Uploaded file is no image.');
        }

        $newFilename = hash_file('sha256', $filePath) . '.jpg';
        $newFilePath = $user->getId() . '/' . $newFilename;

        $fileStream = fopen($filePath, 'r+');
        $this->imageStorage->writeStream($newFilePath, $fileStream);

        return $newFilePath;
    }

    /**
     * @throws FileNotFoundException
     */
    public function removeFile(string $filePath): void
    {
        $this->imageStorage->delete($filePath);
    }

    /**
     * @return false|resource
     * @throws FileNotFoundException
     */
    public function loadFile(string $filePath)
    {
        return $this->imageStorage->read($filePath);
    }
}

