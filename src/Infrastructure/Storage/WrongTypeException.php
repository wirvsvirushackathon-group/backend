<?php
declare(strict_types=1);

namespace App\Infrastructure\Storage;

class WrongTypeException extends \Exception
{
}

