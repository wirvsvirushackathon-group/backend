<?php
declare(strict_types=1);

namespace App\Infrastructure\Validation;

use Symfony\Component\Validator\Constraints as Assert;

class ChallengeCreateValidator extends AbstractCustomerValidator
{
    protected function getConstraints(): array
    {
        return [
            'name' => new Assert\Length(['min' => 5, 'max' => 100]),
            'text' => new Assert\Length(['min' => 5, 'max' => 200]),
        ];
    }
}