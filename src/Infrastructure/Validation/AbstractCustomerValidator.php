<?php
declare(strict_types=1);

namespace App\Infrastructure\Validation;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validation;

abstract class AbstractCustomerValidator
{
    public function validate(array $data): array
    {
        $constraints = $this->getConstraints();

        $violations = [];
        foreach ($constraints as $key => $constraint) {
            if (!isset($data[$key])) {
                $violations[$key] = 'Is missing!';
            }
            $violationMessages = $this->validateSingleEntry($data[$key], $constraint);
            if ($violationMessages !== []) {
                $violations[$key] = $violationMessages;
            }
        }

        return $violations;
    }

    /**
     * @param mixed $value
     * @param Constraint|Constraint[] $constraint
     * @return array
     */
    private function validateSingleEntry($value, $constraint): array
    {
        $validator = Validation::createValidator();
        $constraintValidations = $validator->validate($value, $constraint);

        $violationMessages = [];
        /** @var ConstraintViolationInterface $violation */
        foreach ($constraintValidations as $violation) {
            $violationMessages[] = $violation->getMessage();
        }

        return $violationMessages;
    }

    protected abstract function getConstraints(): array;
}

