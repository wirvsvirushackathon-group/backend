<?php
declare(strict_types=1);

namespace App\Infrastructure\Validation;

use Symfony\Component\Validator\Constraints as Assert;

class UserRegistrationValidator extends AbstractCustomerValidator
{
    protected function getConstraints(): array
    {
        return [
            'email' => new Assert\Email(),
            'username' => new Assert\Length(['max' => 50]),
            'password' => new Assert\Length(['min' => 7]),
        ];
    }
}

