<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200329124438 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE challenge (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary_ordered_time)\', author_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid_binary_ordered_time)\', name VARCHAR(255) NOT NULL, text VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_D70989515E237E06 (name), INDEX IDX_D7098951F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary_ordered_time)\', username VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, roles JSON NOT NULL, passwordHash VARCHAR(255) NOT NULL, points INT NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE challenge_solution (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary_ordered_time)\', challenge_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid_binary_ordered_time)\', submitter_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid_binary_ordered_time)\', image_path VARCHAR(255) NOT NULL, INDEX IDX_20DCE3B98A21AC6 (challenge_id), INDEX IDX_20DCE3B919E5513 (submitter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE challenge ADD CONSTRAINT FK_D7098951F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE challenge_solution ADD CONSTRAINT FK_20DCE3B98A21AC6 FOREIGN KEY (challenge_id) REFERENCES challenge (id)');
        $this->addSql('ALTER TABLE challenge_solution ADD CONSTRAINT FK_20DCE3B919E5513 FOREIGN KEY (submitter_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE challenge_solution DROP FOREIGN KEY FK_20DCE3B98A21AC6');
        $this->addSql('ALTER TABLE challenge DROP FOREIGN KEY FK_D7098951F675F31B');
        $this->addSql('ALTER TABLE challenge_solution DROP FOREIGN KEY FK_20DCE3B919E5513');
        $this->addSql('DROP TABLE challenge');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE challenge_solution');
    }
}
