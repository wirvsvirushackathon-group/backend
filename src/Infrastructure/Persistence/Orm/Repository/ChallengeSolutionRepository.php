<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Orm\Repository;

use App\Domain\Challenge;
use App\Domain\ChallengeSolution;
use App\Domain\ChallengeSolutionId;
use App\Domain\ChallengeSolutionRepositoryInterface;
use App\Domain\User;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;

class ChallengeSolutionRepository implements ChallengeSolutionRepositoryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function persist(ChallengeSolution $challengeSolution): ChallengeSolution
    {
        $this->entityManager->persist($challengeSolution);

        return $challengeSolution;
    }

    public function loadSolution(ChallengeSolutionId $challengeSolutionId): ?ChallengeSolution
    {
        return $this->entityManager->getRepository(ChallengeSolution::class)->find($challengeSolutionId);
    }

    public function loadSolutionsByUserAndChallenge(User $user, Challenge $challenge): ?ChallengeSolution
    {
        return $this->entityManager->getRepository(ChallengeSolution::class)
            ->findOneBy([
                'submitter' => $user,
                'challenge' => $challenge
            ]);
    }

    public function loadList(array $criteria = [], ?int $offset = null, ?int $limit = null): array
    {
        $repository = $this->entityManager->getRepository(ChallengeSolution::class);

        return $repository->findBy($criteria, null, $limit, $offset);
    }

    public function getNextId(): ChallengeSolutionId
    {
        return new ChallengeSolutionId(Uuid::uuid1());
    }

    public function remove(ChallengeSolution $challengeSolution): void
    {
        $this->entityManager->remove($challengeSolution);
    }
}

