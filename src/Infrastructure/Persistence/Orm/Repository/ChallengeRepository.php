<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Orm\Repository;

use App\Domain\Challenge;
use App\Domain\ChallengeId;
use App\Domain\ChallengeRepositoryInterface;
use App\Domain\User;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;

class ChallengeRepository implements ChallengeRepositoryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function persist(Challenge $challenge): Challenge
    {
        $this->entityManager->persist($challenge);

        return $challenge;
    }

    public function loadChallenge(ChallengeId $challengeId): ?Challenge
    {
        return $this->entityManager->find(Challenge::class, $challengeId);
    }

    public function loadChallengesByUser(User $user): array
    {
        return $this->entityManager->getRepository(Challenge::class)
            ->findBy(['author' => $user]);
    }

    public function loadChallengeByName(string $text): ?Challenge
    {
        return $this->entityManager->getRepository(Challenge::class)->findOneBy(['name' => $text]);
    }

    public function loadList(array $criteria = [], ?int $offset = null, ?int $limit = null): array
    {
        $repository = $this->entityManager->getRepository(Challenge::class);
        return $repository->findBy($criteria, null, $limit, $offset);
    }

    public function getNextId(): ChallengeId
    {
        return new ChallengeId(Uuid::uuid1());
    }
}

