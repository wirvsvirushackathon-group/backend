<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Orm\Repository;

use App\Domain\User;
use App\Domain\UserId;
use App\Domain\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserRepository implements UserRepositoryInterface, UserProviderInterface
{
    private EntityManagerInterface $entityManager;
    private UserPasswordEncoderInterface $userPasswordEncoder;
    private JWTTokenManagerInterface $jwtManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $userPasswordEncoder,
        JWTTokenManagerInterface $jwtManager
    ) {
        $this->entityManager = $entityManager;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->jwtManager = $jwtManager;
    }

    public function persist(User $user): User
    {
        $this->entityManager->persist($user);

        return $user;
    }

    public function loadByEmail(string $email): ?User
    {
        $repository = $this->entityManager->getRepository(User::class);
        return $repository->findOneBy(['email' => $email]);
    }

    public function loadByUserName(string $username): ?User
    {
        $repository = $this->entityManager->getRepository(User::class);
        return $repository->findOneBy(['username' => $username]);
    }

    public function loadByUserId(UserId $userId): ?User
    {
        return $this->entityManager->find(User::class, $userId);
    }

    public function getUserToken(User $user): string
    {
        return $this->jwtManager->create($user);
    }

    /**
     * @inheritDoc
     */
    public function loadUserByUsername(string $email)
    {
        return $this->loadByEmail($email);
    }

    /**
     * @inheritDoc
     */
    public function refreshUser(UserInterface $user)
    {
        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * @inheritDoc
     */
    public function supportsClass(string $class)
    {
        if ($class === User::class) {
            return true;
        }
        return false;
    }

    public function getNextId(): UserId
    {
        return new UserId(Uuid::uuid1());
    }
}

