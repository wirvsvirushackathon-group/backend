<?php
declare(strict_types=1);

namespace App\Domain;

class ChallengeSolution
{
    private const EARNED_POINTS = 20;

    private ChallengeSolutionId $id;
    private Challenge $challenge;
    private User $submitter;
    private string $imagePath;

    private function __construct(ChallengeSolutionId $id, User $submitter, Challenge $challenge, string $imagePath)
    {
        $this->id = $id;
        $this->submitter = $submitter;
        $this->challenge = $challenge;
        $this->imagePath = $imagePath;
    }

    public static function submit(ChallengeSolutionId $id, User $submitter, Challenge $challenge, string $imagePath): self
    {
        return new static($id, $submitter, $challenge, $imagePath);
    }

    public function getId(): ChallengeSolutionId
    {
        return $this->id;
    }

    public function getChallenge(): Challenge
    {
        return $this->challenge;
    }

    public function getImagePath(): string
    {
        return $this->imagePath;
    }

    public function getSubmitter(): User
    {
        return $this->submitter;
    }

    public function isSubmitter(User $user): bool
    {
        return $this->submitter->getId()->toString() === $user->getId()->toString();
    }

    public function getEarnedPoints()
    {
        return self::EARNED_POINTS;
    }
}

