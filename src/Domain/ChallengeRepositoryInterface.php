<?php
declare(strict_types=1);

namespace App\Domain;

interface ChallengeRepositoryInterface
{
    public function persist(Challenge $challenge): Challenge;
    public function loadChallenge(ChallengeId $challengeId): ?Challenge;

    /**
     * @return Challenge[]
     */
    public function loadList(array $criteria = [], ?int $offset = null, ?int $limit = null): array;

    /**
     * @return Challenge[]
     */
    public function loadChallengesByUser(User $user): array;

    public function loadChallengeByName(string $text): ?Challenge;

    public function getNextId(): ChallengeId;
}

