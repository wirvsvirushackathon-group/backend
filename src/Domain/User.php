<?php
declare(strict_types=1);

namespace App\Domain;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    private UserId $id;
    private string $username;
    private string $email;
    private string $password;
    private array $roles;
    private int $points;

    public static function register(
        UserId $id,
        string $username,
        string $email,
        string $password
    ) {
        $encodedPassword = password_hash($password, PASSWORD_BCRYPT);
        return new static($id, $username, $email, $encodedPassword, ['ROLE_USER'], 0);
    }

    public static function registerAdmin(
        UserId $id,
        string $username,
        string $email,
        string $password
    ) {
        $adminUser = self::register($id, $username, $email, $password);
        $adminUser->roles = ['ROLE_USER', 'ROLE_ADMIN'];
        return $adminUser;
    }

    private function __construct(UserId $id, string $username, string $email, string $password, array $roles, int $points)
    {
        $this->id = $id;
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
        $this->roles = $roles;
        $this->points = $points;
    }

    public function getId(): UserId
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPoints(): int
    {
        return $this->points;
    }

    public function addPoints(int $points): User
    {
        $this->points = $this->points + $points;

        return $this;
    }

    public function removePoints(int $points): User
    {
        if ($this->points >= $points) {
            $this->points = $this->points - $points;
        }

        if ($this->points < $points) {
            $this->points = 0;
        }

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}

