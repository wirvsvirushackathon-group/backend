<?php
declare(strict_types=1);

namespace App\Domain;

interface ChallengeSolutionRepositoryInterface
{
    public function persist(ChallengeSolution $challengeSolution): ChallengeSolution;

    public function remove(ChallengeSolution $challengeSolution): void;

    public function loadSolution(ChallengeSolutionId $challengeSolutionId): ?ChallengeSolution;

    public function loadSolutionsByUserAndChallenge(User $user, Challenge $challenge): ?ChallengeSolution;

    /**
     * @return ChallengeSolution[]
     */
    public function loadList(array $criteria = [], ?int $offset = null, ?int $limit = null): array;

    public function getNextId(): ChallengeSolutionId;
}

