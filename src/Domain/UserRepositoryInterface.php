<?php
declare(strict_types=1);

namespace App\Domain;

interface UserRepositoryInterface
{
    public function persist(User $user): User;
    public function loadByEmail(string $email): ?User;
    public function loadByUsername(string $username): ?User;
    public function loadByUserId(UserId $userId): ?User;
    public function getUserToken(User $user): string;
    public function getNextId(): UserId;
}

