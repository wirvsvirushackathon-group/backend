<?php
declare(strict_types=1);

namespace App\Domain;

class Challenge
{
    private const EARNED_POINTS = 50;

    private ChallengeId $id;
    private User $author;
    private string $name;
    private string $text;
    private string $type;

    private function __construct(ChallengeId $id, User $author, string $name, string $text, string $type)
    {
        $this->id = $id;
        $this->author = $author;
        $this->name = $name;
        $this->text = $text;
        $this->type = $type;
    }

    public static function submit(ChallengeId $id, User $author, string $name, string $text, string $type): Challenge
    {
        return new static($id, $author, $name, $text, $type);
    }

    public function getId(): ChallengeId
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function getEarnedPoints()
    {
        return self::EARNED_POINTS;
    }
}

