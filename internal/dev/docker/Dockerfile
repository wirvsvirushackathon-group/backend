## Image to build roadrunner (application server) ----------------------------------------------------------------------
FROM golang:1.13 as roadrunner-builder

WORKDIR /app

COPY roadrunner/go.mod roadrunner/go.sum ./

# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

COPY roadrunner .

# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o roadrunner .

## Main application image ----------------------------------------------------------------------------------------------
FROM php:7.4.3-cli-alpine3.11 as main

ENV BLACKFIRE_CLI_VERSION=1.31.0
ENV BLACKFIRE_PHP_EXT_VERSION=1.30.0
ENV GRPC_VERSION=1.27.0
ENV PROTOC_VERSION=3.11.2-r1
ENV PROTOBUF_VERSION=3.11.4
ENV SERVER_PROTOC_VERSION=1.2.0
ENV CLIENT_PROTOC_VERSION=1.27.2
ENV ROAD_RUNNER_VERSION=1.2.0
ENV RDKAFKA_VERSION=4.0.3
ENV LIBRDKAFKA_VERSION=1.2.2-r0
ENV MONGODB_VERSION=1.6.1

# Install packages
RUN apk add --update --no-cache \
    # Some basic stuff
    git \
    shadow \
    unzip \
    zlib-dev \
    libtool \
    oniguruma-dev \
    libxml2-dev \
    libzip-dev \
    openssl-dev \
    autoconf \
    g++ \
    automake \
    make \
    linux-headers \
    ca-certificates

# Install PHP extensions available in docker-php-ext-install; those are:
# bcmath bz2 calendar ctype curl dba dom enchant exif fileinfo filter ftp gd gettext gmp hash iconv imap interbase intl
# json ldap mbstring mysqli oci8 odbc opcache pcntl pdo pdo_dblib pdo_firebird pdo_mysql pdo_oci pdo_odbc pdo_pgsql
# pdo_sqlite pgsql phar posix pspell readline recode reflection session shmop simplexml snmp soap sockets sodium spl
# standard sysvmsg sysvsem sysvshm tidy tokenizer wddx xml xmlreader xmlrpc xmlwriter xsl zend_test zip
RUN docker-php-ext-install \
    pcntl \
    bcmath \
    mbstring \
    pdo_mysql \
    zip

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
# make composer installs faster by parallel downloading
RUN composer require hirak/prestissimo

# Copy built roadrunner server binary
COPY --from=roadrunner-builder /app/roadrunner /usr/local/bin/rr

# fix permissions for files mounted to host - change container's user ID to your host's user ID
# be sure, that your local user has id=1000
RUN usermod -u 1000 www-data

USER www-data

WORKDIR /application