module roadrunner

go 1.13

require (
	github.com/cpuguy83/go-md2man v1.0.10 // indirect
	github.com/spiral/goridge v2.1.4+incompatible // indirect
	github.com/spiral/php-grpc v1.2.0
	github.com/spiral/roadrunner v1.7.0
)
