# HomeNotAlone Backend Server

## Dev Setup


1. Build Docker image:

    `docker-compose build main`
    
2. Run container:

    `docker-compose up -d`
    
3. Generate ssh keys for jwt token generation:

    ```
     $ mkdir -p config/jwt
     $ openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
     $ openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
    ```
   
   - Update your ssh passphrase in .env file
    
4. Install dependencies:

    `docker-compose exec main composer install`
    
5. Update database with migration:

    `docker-compose exec main bin/console doctrine:migrations:migrate`
    
6. Run server in container:

    `docker-compose exec main rr serve -v -d -c roadrunner/config.yaml`
    
7. Access server via:

    `localhost:8080`
    